/*
 * Copyright (c) 2019 Collabora, Ltd.
 * Author: Aguado Puig, Quim <quim.aguado@collabora.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <wayland-client.h>

#include "weston-debug-client-protocol.h"

// TODO: Save stream to properly destroy it before exiting

const char *JSON_STREAM_NAME = "timeline";
const int OUT_FD = STDOUT_FILENO;

struct debug_app {
	struct weston_debug_v1 *debug_iface;
	bool stream_found;
	bool stream_complete;
};

/*
 * Check if the debug json stream is available
 */
static void
debug_available(void *data, struct weston_debug_v1 *debug, const char *name,
		const char *description) {
	struct debug_app *app = data;
	if (strcmp(name, JSON_STREAM_NAME) == 0) {
		fprintf(stderr, "Debug: Stream %s found.\n", name);
		app->stream_found = true;
	}
}

/*
* advertise available debug scope
*
* Advertises an available debug scope which the client may be
* able to bind to. No information is provided by the server about
* the content contained within the debug streams provided by the
* scope, once a client has subscribed.
* @param name debug stream name
* @param description human-readable description of the debug scope
*/
struct weston_debug_v1_listener debug_listener = {
	debug_available,
};

/*
 * Handles the event emited when a global is added into the registry
 */
void global_add(void *data,
		struct wl_registry *registry,
		uint32_t id,
		const char* interface,
		uint32_t version
		) {
	struct debug_app *app = data;


	if (strcmp(interface, weston_debug_v1_interface.name) == 0) {
		if (app->debug_iface) {
			fprintf(stderr, "Warning: Debug global was already added.\n");
			return;
		}
		fprintf(stderr, "Debug: Interface %s found in registry\n", interface);
		app->debug_iface = wl_registry_bind(registry, id,
				&weston_debug_v1_interface, version);
		weston_debug_v1_add_listener(app->debug_iface, &debug_listener, app);
	}
}

/*
 * Handles the event emited when a global is removed from the registry
 */
void global_remove(void *data,
		struct wl_registry *registry,
		uint32_t id) {
	// TODO
}

struct wl_registry_listener registry_listener = {
	.global = global_add,
	.global_remove = global_remove
};

static void
stream_destroy(struct weston_debug_stream_v1 *stream) {
	weston_debug_stream_v1_destroy(stream);
}

static void
handle_stream_complete(void *data, struct weston_debug_stream_v1 *stream) {
	struct debug_app *app = data;
	app->stream_complete = true;
	fprintf(stderr, "Debug: stream complete\n");
	stream_destroy(stream);
}

static void
handle_stream_failure(void *data, struct weston_debug_stream_v1 *stream,
		const char *msg) {
	struct debug_app *app = data;
	app->stream_complete = true;
	fprintf(stderr, "Debug stream %s aborted: %s\n", JSON_STREAM_NAME, msg);
	stream_destroy(stream);
}

static const struct weston_debug_stream_v1_listener stream_listener = {
	handle_stream_complete,
	handle_stream_failure,
};

/*
 * Subscribe to the stream if it exists, and put its output to a file descriptor.
 */
static bool
start_stream(struct debug_app *app) {
	struct weston_debug_stream_v1 *stream;
	if (!app->stream_found) {
		fprintf(stderr, "Can not start unexistent stream %s\n",
				JSON_STREAM_NAME);
		return false;
	}
	stream = weston_debug_v1_subscribe(app->debug_iface,
									JSON_STREAM_NAME,
									OUT_FD);
	weston_debug_stream_v1_add_listener(stream, &stream_listener, app);
	return true;
}

int main(int argc, char *argv[]) {
	// Connect to the Wayland server
	struct wl_display *display = wl_display_connect(NULL);
	if (display == NULL) {
		fprintf(stderr, "Error: Failed to create display.\n");
		return EXIT_FAILURE;
	}

	// The registry ennumerates the globals on the server, which are resources
	// that the server controls.
	struct wl_registry *registry = wl_display_get_registry(display);

	struct debug_app app = {
		.debug_iface = NULL,
		.stream_found = false,
		.stream_complete = false,
	};
	// The registry emits and event every time the server adds or removes a
	// global.
	wl_registry_add_listener(registry, &registry_listener, &app);

	// global_add function is called for each global on the server.
	// XXX: Differences between dispatch and wl_display_roundtrip?
	wl_display_roundtrip(display);

	if (!app.debug_iface) {
		fprintf(stderr,
				"Error: Global \"%s\" does not exists in the registry.\n"\
				"       Make sure weston is running with --debug flag\n",
				weston_debug_v1_interface.name);
		return EXIT_FAILURE;
	}

	wl_display_roundtrip(display);

	if (!start_stream(&app)) {
		return EXIT_FAILURE;
	}

	weston_debug_v1_destroy(app.debug_iface);

	while (1) {
		if (!app.stream_found || app.stream_complete) break;
		if (wl_display_dispatch(display) < 0) {
			// TODO: Read errno
			fprintf(stderr, "Error: Could not dispatch more events.");
			return EXIT_FAILURE;
		}
	}
}
